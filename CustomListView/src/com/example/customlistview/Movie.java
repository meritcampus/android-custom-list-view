package com.example.customlistview;

public class Movie {
	String movieName;
	String director;
	String musicDirector;
	private String heroName;
	private int imageId;
	public String getName() {
		return movieName;
	}
	public void setMovieName(String name) {
		this.movieName = name;
	}
	public String getMovieName() {
		return movieName;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getMusicDirector() {
		return musicDirector;
	}
	public void setMusicDirector(String musicDirector) {
		this.musicDirector = musicDirector;
	}
	public String getHeroName() {
		return heroName;
	}
	public void setHeroName(String heroName) {
		this.heroName = heroName;
	}
	public int getImageId() {
		return imageId;
	}
	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
	
	
	

}
