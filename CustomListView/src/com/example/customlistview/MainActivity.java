package com.example.customlistview;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class MainActivity extends Activity implements OnItemClickListener{
	ListView listview;
	List<Movie> movieList;
	// create custom adapter class pass list to that class
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listview = (ListView) findViewById(R.id.listView);
		 movieList = getMovieList();
		CustomAdapter adapter=new CustomAdapter(getApplicationContext(), R.layout.list_row, movieList);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(this);

	}

	public List<Movie> getMovieList() {
		List<Movie> movieList = new ArrayList<Movie>();
		Movie chennaiExpress = new Movie();

		chennaiExpress.setMovieName("ChennaiExpress");
		chennaiExpress.setDirector("RohitShetty");
		chennaiExpress
				.setMusicDirector(" Vishal Dadlani, Vishal-Shekhar, Shekhar Ravjiani");
		chennaiExpress.setHeroName("ShahRukh Khan");
		chennaiExpress.setImageId(R.drawable.chennaiex);

		movieList.add(chennaiExpress);
		Movie dhoom3 = new Movie();
		dhoom3.setMovieName("Dhoom3");
		dhoom3.setHeroName("AamirKhan");
		dhoom3.setDirector("Vijay Krishna Acharya");
		dhoom3.setMusicDirector(" Pritam");
		dhoom3.setImageId(R.drawable.dhoom);
		movieList.add(dhoom3);
		Movie pk = new Movie();
		pk.setMovieName("PK");
		pk.setHeroName("AamirKhan");
		pk.setDirector("Rajkumar Hirani");
		pk.setMusicDirector(" Ankit Tiwari");
		pk.setImageId(R.drawable.pk);
		movieList.add(pk);
		Movie happynewYear = new Movie();
		happynewYear.setMovieName("HappynewYear");
		happynewYear.setDirector("Farah Khan");
		happynewYear.setMusicDirector("Vishal Dadlani");
		happynewYear.setHeroName("Shah Rukh Khan");
		happynewYear.setImageId(R.drawable.hpynewyr);
		movieList.add(happynewYear);
		Movie threeIdiots = new Movie();
		threeIdiots.setMovieName("3idiots");
		threeIdiots.setDirector("Rajkumar Hirani");
		threeIdiots.setMusicDirector("Shantanu Moitra");
		threeIdiots.setHeroName("AamirKhan");
		threeIdiots.setImageId(R.drawable.idiot);
		movieList.add(threeIdiots);
		Movie bahubali = new Movie();
		bahubali.setMovieName("Bahubali");
		bahubali.setDirector("Rajamouli");
		bahubali.setMusicDirector("kiravani");
		bahubali.setHeroName("Prabhas");
		bahubali.setImageId(R.drawable.bahubali);
		movieList.add(bahubali);
		return movieList;

	}
// when click on list row this method will call
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Movie movie = movieList.get(position);
		Toast.makeText(getApplicationContext(), "Clicked on item "+ movie.getMovieName() +" "+movie.getHeroName() , Toast.LENGTH_SHORT).show();
		
	}

}
