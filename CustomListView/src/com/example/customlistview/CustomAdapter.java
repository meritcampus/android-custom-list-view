package com.example.customlistview;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class CustomAdapter extends BaseAdapter {
	List<Movie> movieList = new ArrayList<Movie>();
	Context context;
	int resourceId;

	CustomAdapter(Context context, int id, List<Movie> list) {
		this.context = context;
		this.resourceId = id;
		this.movieList = list;

	}

	@Override
	public int getCount() {

		return movieList.size();
	}

	@Override
	public Movie getItem(int position) {

		return movieList.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.list_row, null);
		// get id from xml file
		TextView movieHero = (TextView)view.findViewById(R.id.movie_hero);
		TextView movieDirector = (TextView)view.findViewById(R.id.movie_director);
		TextView movieMusicDirector = (TextView)view.findViewById(R.id.movie_music);
		TextView movieName = (TextView)view.findViewById(R.id.movie_name);
		ImageView imageView = (ImageView)view.findViewById(R.id.movie_image);
		LinearLayout mainLayout = (LinearLayout)view.findViewById(R.id.main_layout);
		// set data one by one
		movieHero.setText(movieList.get(position).getHeroName());
		movieDirector.setText(movieList.get(position).getDirector());
		movieMusicDirector.setText(movieList.get(position).getMusicDirector());
		movieName.setText(movieList.get(position).getName());
		imageView.setImageResource(movieList.get(position).getImageId());
		// onclick for list item
		mainLayout.setOnClickListener(createOnClickListener(position, parent));
		return view;
	}
	public OnClickListener createOnClickListener(final int position, final ViewGroup parent) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 0);
            }
        };
    }

	

}
